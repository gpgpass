// SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
// SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "passentry.h"

#include <QSignalSpy>
#include <QTest>

class TestPassEntry : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void readPassword();
};

void TestPassEntry::readPassword()
{
    PassEntry::NamedValue key = {QStringLiteral("key"), QStringLiteral("val")};
    PassEntry::NamedValue key2 = {QStringLiteral("key2"), QStringLiteral("val2")};
    QString password = QStringLiteral("password");

    PassEntry entry(QStringLiteral("name"), QStringLiteral("password\n"), {}, false);
    QCOMPARE(entry.password(), password);
    QCOMPARE(entry.namedValues(), {});
    QCOMPARE(entry.remainingData(), QString());

    entry = PassEntry(QStringLiteral("name"), QStringLiteral("password"), {}, false);
    QCOMPARE(entry.password(), password);
    QCOMPARE(entry.namedValues(), {});
    QCOMPARE(entry.remainingData(), QString());

    entry = PassEntry(QStringLiteral("name"), QStringLiteral("password\nfoobar\n"), {}, false);
    QCOMPARE(entry.password(), password);
    QCOMPARE(entry.namedValues(), {});
    QCOMPARE(entry.remainingData(), QStringLiteral("foobar\n"));

    entry = PassEntry(QStringLiteral("name"), QStringLiteral("password\nkey: val\nkey2: val2"), {QStringLiteral("key2")}, false);
    QCOMPARE(entry.password(), password);
    QCOMPARE(entry.namedValues(), QList<PassEntry::NamedValue>{key2});
    QCOMPARE(entry.remainingData(), QStringLiteral("key: val"));

    entry = PassEntry(QStringLiteral("name"), QStringLiteral("password\nkey: val\nkey2: val2"), {QStringLiteral("key2")}, true);
    QCOMPARE(entry.password(), password);
    QCOMPARE(entry.namedValues(), PassEntry::NamedValues({key, key2}));
    QCOMPARE(entry.remainingData(), QString());
}

QTEST_MAIN(TestPassEntry)
#include "tst_passentry.moc"
