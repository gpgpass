# Tests

To edit the key included in this gnupg demo, run:

```bash
GNUPGHOME=~/path/to/gpgpass/tests/gnupg_home gpg-agent --daemon kleopatra
```
