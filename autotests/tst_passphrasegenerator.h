// SPDX-FileCopyrightText: 2019 KeePassXC Team <team@keepassxc.org>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include <QObject>

class TestPassphraseGenerator : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void testWordCase();
};
