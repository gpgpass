/*
    SPDX-FileCopyrightText: 2024 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef ADDFILEINFOPROXY_H
#define ADDFILEINFOPROXY_H

#include <QIdentityProxyModel>

class QFileSystemModel;

/// Once we have a Qt with https://codereview.qt-project.org/c/qt/qtbase/+/550138
/// This entire class can be removed
class AddFileInfoProxy : public QIdentityProxyModel
{
    Q_OBJECT
public:
    static const int FileInfoRole = Qt::UserRole + 10;
    explicit AddFileInfoProxy(QObject *parent = nullptr);
    QVariant data(const QModelIndex &index, int role) const override;
    void setSourceModel(QAbstractItemModel *model) override;

private:
    QFileSystemModel *m_model = nullptr;
};

#endif // ADDFILEINFOPROXY_H
