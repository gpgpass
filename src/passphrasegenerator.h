// SPDX-FileCopyrightText: 2017 KeePassXC Team <team@keepassxc.org>
// SPDX-License-Identifier: GPL-2.0-only or GPL-3.0-only

#pragma once

#include <QVector>

class PassphraseGenerator
{
public:
    PassphraseGenerator();
    Q_DISABLE_COPY(PassphraseGenerator)

    enum PassphraseWordCase { LOWERCASE, UPPERCASE, TITLECASE };

    double estimateEntropy(int wordCount = 0);
    void setWordCount(int wordCount);
    void setWordList(const QString &path);
    void setWordCase(PassphraseWordCase wordCase);
    void setDefaultWordList();
    void setWordSeparator(const QString &separator);
    bool isValid() const;

    QString generatePassphrase() const;

    static constexpr int DefaultWordCount = 7;
    static const QLatin1String DefaultSeparator;
    static const QLatin1String DefaultWordList;

private:
    int m_wordCount;
    PassphraseWordCase m_wordCase;
    QString m_separator;
    QVector<QString> m_wordlist;
};
