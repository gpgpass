// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#include <QtGlobal>
#include <libkleo_version.h>
#define KLEO_HAS_CERTIFICATE_CREATION_DIALOG LIBKLEO_VERSION >= QT_VERSION_CHECK(6, 2, 40)

#if KLEO_HAS_CERTIFICATE_CREATION_DIALOG
#include <KJob>
#include <QGpgME/Job>
#include <gpgme++/interfaces/passphraseprovider.h>

namespace Kleo
{
class KeyParameters;
}

namespace GpgME
{
class KeyGenerationResult;
}

class EmptyPassphraseProvider : public GpgME::PassphraseProvider
{
public:
    char *getPassphrase(const char *useridHint, const char *description, bool previousWasBad, bool &canceled) override;
};

class OpenPGPCertificateCreationJob : public KJob
{
    Q_OBJECT

public:
    enum {
        GpgError = UserDefinedError,
    };
    explicit OpenPGPCertificateCreationJob(const QString &name, const QString &email, QWidget *parent);
    ~OpenPGPCertificateCreationJob() override;

    QString fingerprint() const;

    bool doKill() override;
    void start() override;

private:
    void createCertificate(const Kleo::KeyParameters &keyParameters, bool protectKeyWithPassword);
    void keyGenerated(const GpgME::KeyGenerationResult &result);
    const QString mName;
    const QString mEmail;
    QGpgME::Job *mJob = nullptr;
    EmptyPassphraseProvider emptyPassphraseProvider;
    QString mFingerprint;
};
#endif
