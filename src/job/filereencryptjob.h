// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include <KCompositeJob>

/// Decrypt and then reencrypt a file with new recipients
class FileReencryptJob : public KCompositeJob
{
    Q_OBJECT

public:
    explicit FileReencryptJob(const QString &filePath, const QList<QByteArray> recipients, QObject *parent = nullptr);

    void start() override;

private:
    QString m_filePath;
    const QList<QByteArray> m_recipients;
};
