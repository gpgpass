/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#include "usersdialog.h"
#include "models/userslistmodel.h"
#include "ui_usersdialog.h"
#include "ui_userswidget.h"
#include <KLocalizedString>
#include <QMessageBox>
#include <QRegularExpression>
#include <QWidget>

/**
 * @brief UsersDialog::UsersDialog basic constructor
 * @param parent
 */
UsersDialog::UsersDialog(const QList<QByteArray> &recipients, QWidget *parent)
    : QDialog(parent)
    , dialogUi(std::make_unique<Ui_UsersDialog>())
    , ui(std::make_unique<Ui_UsersWidget>())
    , m_usersListModel(new UsersListModel(recipients, this))
    , m_filterUsersListModel(new FilterUsersListModel(this))
{
    dialogUi->setupUi(this);
    ui->setupUi(dialogUi->widget);
    ui->listView->setModel(m_filterUsersListModel);

    m_filterUsersListModel->setSourceModel(m_usersListModel);
    m_filterUsersListModel->setShowOnlyValidUser(!ui->checkBox->isChecked());
    m_filterUsersListModel->setFilterWildcard(ui->lineEdit->text());
    connect(ui->checkBox, &QCheckBox::stateChanged, ui->checkBox, [this]() {
        m_filterUsersListModel->setShowOnlyValidUser(!ui->checkBox->isChecked());
    });
    connect(ui->lineEdit, &QLineEdit::textChanged, ui->lineEdit, [this]() {
        m_filterUsersListModel->setFilterWildcard(ui->lineEdit->text());
    });

    connect(m_usersListModel, &UsersListModel::errorOccurred, this, [parent](const QString &error) {
        QMessageBox::critical(parent, i18n("Keylist missing"), i18n("Could not fetch list of available GPG keys.") + u' ' + error);
    });

    connect(dialogUi->buttonBox, &QDialogButtonBox::accepted, this, &UsersDialog::accept);
    connect(dialogUi->buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    ui->lineEdit->setClearButtonEnabled(true);
}

/**
 * @brief UsersDialog::~UsersDialog basic destructor.
 */
UsersDialog::~UsersDialog() = default;

Q_DECLARE_METATYPE(UserInfo *)

/**
 * @brief UsersDialog::accept
 */
void UsersDialog::accept()
{
    bool hasSecretSelected = false;
    const auto users = m_usersListModel->users();
    QList<QByteArray> keys;
    for (const UserInfo &user : users) {
        if (user.enabled) {
            hasSecretSelected |= user.have_secret;
            keys << user.key_id.toUtf8();
        }
    }

    // only overwrite .gpg-id if at least a secret is selected
    if (!hasSecretSelected) {
        showError(
            i18n("None of the selected keys have a secret key available.\n"
                 "You will not be able to decrypt any newly added passwords!"));
        return;
    }

    Q_EMIT save(keys);
}

void UsersDialog::showError(const QString &errorText)
{
    ui->messageWidget->setText(errorText);
    ui->messageWidget->animatedShow();
}
