// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include "adjustingscrollarea.h"
#include <QTimer>

class QMainWindow;
class QFormLayout;
class QPushButton;
class QStackedLayout;
class KTitleWidget;
class ClipboardHelper;
class PassEntry;
class KJob;
class KPlaceholderWidget;

/// Widget to view passwords
class PasswordViewerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PasswordViewerWidget(ClipboardHelper *clipboardHelper, QWidget *parent = nullptr);

    QString filePath() const;
    void setFilePath(const QString &entryName, const QString &filePath);

    QString rawContent() const;
    void setRawContent(const QString &rawContent);

    void setPassEntry(const PassEntry &passEntry, const QString &content);

    void setPanelTimer();
    void setSearchBarHeight(int searchBarHeight);

    /// Hide the information from shoulder surfers
    void clear();

    void setErrorString();

Q_SIGNALS:
    void loaded();

private:
    void fileDecryptedSlot(KJob *job);

    /// Add one field to the template grid
    void addToGridLayout(const QString &field, const QString &value);

    /// Empty the template widget fields in the UI.
    void clearTemplateWidgets();

    void decryptFile();

    QString m_entryName;
    QString m_filePath;
    QString m_rawContent;
    KPlaceholderWidget *const m_placeholderWidget;
    KTitleWidget *const m_titleWidget;
    ClipboardHelper *const m_clipboardHelper;
    QFormLayout *const m_contentLayout;
    QPushButton *const m_copyPasswordNameButton;
    QStackedLayout *m_stackedLayout;
    QTimer clearPanelTimer;
};
