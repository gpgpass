// SPDX-FileCopyrightText: 2023 g10 Code GmbH
// SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>
// SPDX-FileContributor: Carl Schwan <carl.schwan@gnupg.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QFrame>

class QrCodePopup : public QFrame
{
public:
    explicit QrCodePopup(const QString &value, QWidget *parent);

public Q_SLOTS:
    void show();

protected:
    bool event(QEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *event) override;
};
