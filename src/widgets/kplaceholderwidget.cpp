// SPDX-FileCopyrightText: 2025 g10 Code GmbH
// SPDX-FileContributor: Tobias Fella <tobias.fella@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "kplaceholderwidget.h"

#include <KTitleWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

class KPlaceholderWidget::Private
{
public:
    QLabel *icon;
    QVBoxLayout *layout;
    KTitleWidget *title;
    QLabel *label;
    QVBoxLayout *actionsLayout;
};

KPlaceholderWidget::KPlaceholderWidget(QWidget *parent)
    : QWidget(parent)
    , d(std::make_unique<Private>())
{
    d->layout = new QVBoxLayout(this);
    d->layout->addStretch();
    d->icon = new QLabel(this);
    d->icon->setAlignment(Qt::AlignHCenter);
    d->layout->addWidget(d->icon);
    d->title = new KTitleWidget(this);

    d->layout->addWidget(d->title, 0, Qt::AlignHCenter);
    d->label = new QLabel();
    d->label->setAlignment(Qt::AlignHCenter);
    d->layout->addWidget(d->label);

    d->actionsLayout = new QVBoxLayout();
    d->layout->addLayout(d->actionsLayout);
    d->layout->addStretch();
}

KPlaceholderWidget::~KPlaceholderWidget() = default;

void KPlaceholderWidget::setTitle(const QString &title)
{
    d->title->setText(title);
}

QString KPlaceholderWidget::title() const
{
    return d->title->text();
}

void KPlaceholderWidget::setText(const QString &text)
{
    d->label->setText(text);
}

QString KPlaceholderWidget::text() const
{
    return d->label->text();
}

void KPlaceholderWidget::setIcon(const QString &iconName)
{
    d->icon->setPixmap(QIcon::fromTheme(iconName).pixmap(128, 128));
}

void KPlaceholderWidget::addAction(QAction *action)
{
    auto button = new QPushButton(action->icon(), action->text(), this);
    d->actionsLayout->addWidget(button);
    connect(button, &QPushButton::clicked, action, &QAction::triggered);
}
