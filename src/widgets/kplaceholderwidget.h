// SPDX-FileCopyrightText: 2025 g10 Code GmbH
// SPDX-FileContributor: Tobias Fella <tobias.fella@gnupg.com>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QWidget>

class KPlaceholderWidget : public QWidget
{
    Q_OBJECT

public:
    explicit KPlaceholderWidget(QWidget *parent = nullptr);
    ~KPlaceholderWidget();

    void setTitle(const QString &title);
    QString title() const;

    void setText(const QString &text);
    QString text() const;

    void setIcon(const QString &iconName);

    void addAction(QAction *action);

private:
    class Private;
    std::unique_ptr<Private> d;
};
