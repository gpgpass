/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef UTIL_H_
#define UTIL_H_

#include <QRegularExpression>
#include <QString>

class StoreModel;

/*!
    \class Util
    \brief Some static utilities to be used elsewhere.
 */
class Util
{
public:
    static QString findPasswordStore();
    static QString normalizeFolderPath(QString path);
    static void copyDir(const QString &src, const QString &dest);
    static const QRegularExpression &endsWithGpg();
    static const QRegularExpression &protocolRegex();

private:
};

#endif // UTIL_H_
