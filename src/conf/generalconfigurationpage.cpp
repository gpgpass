// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "generalconfigurationpage.h"
#include "config.h"
#include "ui_generalconfigurationpage.h"

using namespace GpgPass::Config;

GeneralConfigurationPage::GeneralConfigurationPage(QWidget *parent)
    : GpgPassConfigModule(parent)
    , ui(new Ui::GeneralConfigurationPage)
{
    ui->setupUi(this);
    ui->mainLayout->insertStretch(0);
    ui->mainLayout->insertStretch(2);

    connect(ui->checkBoxAutoclearPanel, &QAbstractButton::toggled, this, &GeneralConfigurationPage::setAutoClearPanelSubentries);
    connect(ui->checkBoxAutoclear, &QAbstractButton::toggled, this, &GeneralConfigurationPage::setAutoClearSubentries);

    // Changed
    connect(ui->checkBoxAutoclear, &QAbstractButton::toggled, this, &GeneralConfigurationPage::changed);
    connect(ui->checkBoxAutoclearPanel, &QAbstractButton::toggled, this, &GeneralConfigurationPage::changed);
    connect(ui->checkBoxNoLineWrapping, &QAbstractButton::toggled, this, &GeneralConfigurationPage::changed);
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
    connect(ui->spinBoxAutoclearPanelSeconds, &QSpinBox::valueChanged, this, &GeneralConfigurationPage::changed);
    connect(ui->spinBoxAutoclearPanelSeconds, &QSpinBox::valueChanged, this, &GeneralConfigurationPage::changed);
#else
    connect(ui->spinBoxAutoclearPanelSeconds, QOverload<int>::of(&QSpinBox::valueChanged), this, &GeneralConfigurationPage::changed);
    connect(ui->spinBoxAutoclearPanelSeconds, QOverload<int>::of(&QSpinBox::valueChanged), this, &GeneralConfigurationPage::changed);
#endif
}

void GeneralConfigurationPage::save()
{
    auto config = ::Config::self();
    config->setClipboardAutoClearEnabled(ui->checkBoxAutoclear->isChecked());
    config->setClipboardAutoClearTime(ui->spinBoxAutoclearSeconds->value());
    config->setViewerAutoClearEnabled(ui->checkBoxAutoclearPanel->isChecked());
    config->setViewerAutoClearTime(ui->spinBoxAutoclearPanelSeconds->value());
    config->setNoLineWrapping(ui->checkBoxNoLineWrapping->isChecked());
    config->save();
}

void GeneralConfigurationPage::load()
{
    const auto config = ::Config::self();
    ui->spinBoxAutoclearSeconds->setValue(config->clipboardAutoClearTime());
    setAutoclear(config->clipboardAutoClearEnabled());

    ui->spinBoxAutoclearPanelSeconds->setValue(config->viewerAutoClearTime());
    setAutoclearPanel(config->viewerAutoClearEnabled());

    ui->checkBoxNoLineWrapping->setChecked(config->noLineWrapping());
}

void GeneralConfigurationPage::defaults()
{
    auto config = ::Config::self();
    ui->spinBoxAutoclearSeconds->setValue(config->defaultClipboardAutoClearTimeValue());
    setAutoclear(config->defaultClipboardAutoClearEnabledValue());

    ui->spinBoxAutoclearPanelSeconds->setValue(config->defaultViewerAutoClearTimeValue());
    setAutoclearPanel(config->defaultViewerAutoClearEnabledValue());

    ui->checkBoxNoLineWrapping->setChecked(config->defaultNoLineWrappingValue());

    Q_EMIT changed();
}

void GeneralConfigurationPage::setAutoClearSubentries(bool autoclear)
{
    ui->spinBoxAutoclearSeconds->setEnabled(autoclear);
}

void GeneralConfigurationPage::setAutoclear(bool autoclear)
{
    ui->checkBoxAutoclear->setChecked(autoclear);
    setAutoClearSubentries(autoclear);
}

void GeneralConfigurationPage::setAutoclearPanel(bool autoclear)
{
    ui->checkBoxAutoclearPanel->setChecked(autoclear);
    setAutoClearPanelSubentries(autoclear);
}

void GeneralConfigurationPage::setAutoClearPanelSubentries(bool autoclear)
{
    ui->spinBoxAutoclearPanelSeconds->setEnabled(autoclear);
    ui->labelPanelSeconds->setEnabled(autoclear);
}
