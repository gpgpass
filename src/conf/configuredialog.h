// SPDX-FileCopyrightText: 2024 g10 Code GmbH
// SPDX-FileContributor: Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "gpgpasspageconfigdialog.h"

class RootFoldersManager;
class KPageWidgetItem;

class ConfigureDialog : public GpgPassPageConfigDialog
{
    Q_OBJECT
public:
    explicit ConfigureDialog(RootFoldersManager *rootfoldersmanager, QWidget *parent = nullptr);

    enum class Page {
        None,
        General,
        PasswordsStore,
        Template,
    };

    void openPage(Page page);

protected:
    void hideEvent(QHideEvent *) override;

private:
    KPageWidgetItem *m_generalPage;
    KPageWidgetItem *m_passwordStoresPage;
    KPageWidgetItem *m_templatePage;
};
