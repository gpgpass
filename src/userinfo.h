/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef DATAHELPERS_H
#define DATAHELPERS_H

#include <QDateTime>
#include <QObject>
#include <QString>

/*!
    \struct UserInfo
    \brief Stores key info lines including validity, creation date and more.
 */
struct UserInfo {
    Q_GADGET
public:
    UserInfo()
        : validity('-')
        , have_secret(false)
        , enabled(false)
    {
    }

    /**
     * @brief UserInfo::fullyValid when validity is f or u.
     * http://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob_plain;f=doc/DETAILS
     */
    bool fullyValid() const
    {
        return validity == 'f' || validity == 'u';
    }
    /**
     * @brief UserInfo::marginallyValid when validity is m.
     * http://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob_plain;f=doc/DETAILS
     */
    bool marginallyValid() const
    {
        return validity == 'm';
    }
    /**
     * @brief UserInfo::isValid when fullyValid or marginallyValid.
     */
    bool isValid() const
    {
        return fullyValid() || marginallyValid();
    }

    bool isExpired() const
    {
        return expiry.toSecsSinceEpoch() > 0 && expiry.daysTo(QDateTime::currentDateTime()) > 0;
    }

    /**
     * @brief UserInfo::name full name
     */
    QString name;
    /**
     * @brief UserInfo::key_id hexadecimal representation
     */
    QString key_id;
    /**
     * @brief UserInfo::validity GnuPG representation of validity
     * http://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob_plain;f=doc/DETAILS
     */
    char validity;
    /**
     * @brief UserInfo::have_secret secret key is available
     * (can decrypt with this key)
     */
    bool have_secret;
    /**
     * @brief UserInfo::enabled
     */
    bool enabled;
    /**
     * @brief UserInfo::expiry date/time key expires
     */
    QDateTime expiry;
    /**
     * @brief UserInfo::created date/time key was created
     */
    QDateTime created;
};

#endif // DATAHELPERS_H
